//
//  CircleViewController.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 9/11/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit

class CircleViewController:UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout  {

    @IBOutlet weak var titleView: UILabel!
    
    @IBOutlet weak var addBtn: UIButton!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var fam = [UserParticipant] ()
    var guest = [UserParticipant] ()
    
    override func viewWillLayoutSubviews() {
        addBtn.roundCorners(radius: addBtn.frame.size.height / 2)
    }
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        titleView.textColor = K.UI.main_color
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib.init(nibName: "CirclePersonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CirclePersonCollectionViewCell")
        
        
        if(K.Us.currentCircle != nil){
            
            if let dat = K.Us.currentCircle?.getParticipants() {
                fam = dat
                collectionView.reloadData()
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(K.Us.currentCircle != nil){
            
            if let dat = K.Us.currentCircle?.getParticipants() {
                fam = dat
                collectionView.reloadData()
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width , height: self.collectionView.frame.height / 5 * 0.8 )
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (section == 0) ? fam.count : guest.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //If you are using multiple cells based on section make condition
        
        if indexPath.section == 0 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CirclePersonCollectionViewCell", for: indexPath) as! CirclePersonCollectionViewCell
            cell.phone = fam[indexPath.row].phone ?? ""
            cell.callBtn.addTarget(self, action: #selector(buttonClicked(sender:)), for: .touchUpInside)
            cell.callBtn.tag = indexPath.row
            cell.nameLb.text = fam[indexPath.row].name
            if(indexPath.row % 2 == 0){
             cell.statusLb.textColor = K.UI.third_color
                cell.statusLb.text = "Afuera de casa"
            }
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CirclePersonCollectionViewCell", for: indexPath) as! CirclePersonCollectionViewCell
            cell.nameLb.text = guest[indexPath.row].name
            if(indexPath.row % 2 == 0){
                cell.statusLb.textColor = K.UI.third_color
                cell.statusLb.text = "Afuera de casa"
            }
            return cell
        }
        
    }
    
    
    @objc func buttonClicked(sender: UIButton){
        
        if let ph = fam[sender.tag].phone {
            let url = URL(string: "tel://" + ph)
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url! , options: [:] , completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url!)
                // Fallback on earlier versions
            }
        }
    }

    @IBAction func addParticipant(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NewParticipantViewController") as! NewParticipantViewController
        self.present(vc, animated: true, completion: nil)
    }
    
}
