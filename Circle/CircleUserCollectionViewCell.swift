//
//  CircleObjectCollectionViewCell.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 9/11/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit

class CircleUserCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var statusLb: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var callView: UIView!
 
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        self.mainView.layoutIfNeeded() // Add this line
        userImg.circleImage()
        mainView.roundCorners(radius: 4.0)
        mainView.mainshadow(color: K.UI.shadow_color)
        callView.mainshadow(color: K.UI.main_color)
        callView.roundCorners(radius: 4.0)
        
    }

}
