//
//  NewParticipantViewController.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 10/8/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit

class NewParticipantViewController: UIViewController {

    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var userIDTxt: UITextField!
    @IBOutlet weak var addBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleView.textColor = K.UI.main_color

        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        addBtn.roundCorners(radius: addBtn.frame.height / 2)
        addBtn.addCancelShadow()
        addBtn.layer.shadowRadius = addBtn.frame.size.height / 2
    }
    @IBAction func back(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addParticipant(_ sender: Any) {
        
        if (userIDTxt.text != nil && userIDTxt.text != ""){
            
            User.withID(id: userIDTxt.text!) { (user) in
                if user != nil {
                    
                    if (K.Us.currentCircle != nil){
                        let part = UserParticipant([:])
                        part.name = user?.name
                        
                        part.phone = user?.phone
                        
                        part.ref = K.FireStore.ref().collection("users").document(self.userIDTxt.text!)
                        part.objectID = self.userIDTxt.text!
                        K.Us.currentCircle?.addParticipant(participant: part)
                        K.Us.currentCircle?.save(route: "circles")
                        
                        let cir = UserCircle([:])
                        cir.name = K.Us.currentCircle?.name
                        if let u = K.Us.currentCircle?.uid {
                            cir.objectID = u
                            cir.ref = K.FireStore.ref().collection("circles").document(u)
                            user?.addCircle(circle: cir)
                            
                            let alert = UIAlertController(title: "Saved", message: "User with id \( self.userIDTxt.text!) was added to circle.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    self.userIDTxt.text = ""
                                    self.dismiss(animated: true, completion: nil)
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                    
                                    
                                }}))
                            self.present(alert, animated: true, completion: nil)
                        }
                       
                        
                    }
                    
                }else {
                    let alert = UIAlertController(title: "Bad ID", message: "We dont have a user with id \( self.userIDTxt.text!).", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        switch action.style{
                        case .default:
                            self.userIDTxt.text = ""
                            self.dismiss(animated: true, completion: nil)
                            
                        case .cancel:
                            print("cancel")
                            
                        case .destructive:
                            print("destructive")
                            
                            
                        }}))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
