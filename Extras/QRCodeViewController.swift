//
//  QRCodeViewController.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 10/8/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit

class QRCodeViewController: UIViewController {

    @IBOutlet weak var imageQR: UIImageView!
    
    @IBOutlet weak var titleView: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleView.textColor = K.UI.main_color
        
        if (K.Us.user?.uid != nil){
            let data = K.Us.user?.uid?.data(using: .ascii, allowLossyConversion: false)
            let filter = CIFilter(name: "CIQRCodeGenerator")
            filter?.setValue(data, forKey: "inputMessage")
            filter?.setValue("H", forKey: "inputCorrectionLevel")
           
            
            if let output = filter?.outputImage{
                
                let scaleX = imageQR.frame.size.width / output.extent.size.width
                
                 let scaley = imageQR.frame.size.height / output.extent.size.height
                
                let transform = CGAffineTransform(scaleX: scaleX, y: scaley)
                
                if let ou = filter?.outputImage?.transformed(by: transform) {
                    
                    let image = UIImage(ciImage: ou)
                    imageQR.image = image
                    
                }else{
                    let image = UIImage(ciImage: output)
                    imageQR.image = image
                    
                }
                
                
            }
            
           
            
            
            
            
        }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
