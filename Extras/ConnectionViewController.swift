
//
//  ConnectionViewController.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 10/8/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class ConnectionViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let connectedRef =
            Firebase.Database.database().reference(withPath: ".info/connected")
        connectedRef.observe(.value, with: { snapshot in
            if snapshot.value as? Bool ?? false {
                self.dismiss(animated: true, completion: nil)
            }
        })

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
