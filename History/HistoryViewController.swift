//
//  HistoryViewController.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 9/11/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit
import MBProgressHUD

class HistoryViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleView: UILabel!
    
    var list = [Event] ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
         titleView.textColor = K.UI.main_color
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib.init(nibName: "HistoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HistoryCollectionViewCell")
        
        if (K.Us.currentCircle != nil){
             MBProgressHUD.showAdded(to: self.view, animated: true)
            K.Us.currentCircle?.getHistory(callback: { (events) in
                if events != nil {
                  self.list = events!
                    self.collectionView.reloadData()
                }
                
                MBProgressHUD.hide(for: self.view, animated: true)
            })
        }

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (K.Us.currentCircle != nil){
            MBProgressHUD.showAdded(to: self.view, animated: true)
            K.Us.currentCircle?.getHistory(callback: { (events) in
                if events != nil {
                    self.list = events!
                    self.collectionView.reloadData()
                }
                
                MBProgressHUD.hide(for: self.view, animated: true)
            })
        }

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HistoryCollectionViewCell", for: indexPath) as! HistoryCollectionViewCell
        
        cell.mainTitleLb.text = list[indexPath.row].name
        cell.dateLb.text = list[indexPath.row].date?.toCalendar()
       
        if (list[indexPath.row].action == 0){
             cell.actionLb.textColor = UIColor.red
             cell.actionLb.text = "Salio"
        }else{
             cell.actionLb.text = "Entro"
        }
        
        if (list[indexPath.row].objectType == 1 ){
            cell.iconImg.image = UIImage(named: "CircleGrey")
        }else{
            cell.iconImg.image = UIImage(named: "Obj3")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width , height: self.collectionView.frame.height / 5 * 0.8 )
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
