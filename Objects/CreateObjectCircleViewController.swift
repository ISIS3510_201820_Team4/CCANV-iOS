//
//  CreateObjectCircleViewController.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 10/8/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit

class CreateObjectCircleViewController: UIViewController , UIPickerViewDataSource, UIPickerViewDelegate{
    
    
    @IBOutlet weak var objectImage: UIImageView!
    
    @IBOutlet weak var nameTxt: UITextField!
    
    @IBOutlet weak var imageTxt: UITextField!
    
    @IBOutlet weak var addBtn: UIButton!
    
    @IBOutlet weak var titleView: UILabel!
    
    var images = [UIImage(named: "Obj1") , UIImage(named: "Obj3")]
    var names = ["Jewelry" , "technology"]
    
    var selectImage : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleView.textColor = K.UI.main_color
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.backgroundColor = UIColor.white
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancel))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        
        imageTxt.inputAccessoryView = toolBar
        
        imageTxt.inputView = pickerView
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        objectImage.circleImage()
        addBtn.roundCorners(radius: addBtn.frame.size.height / 2)
        addBtn.addCancelShadow()
        addBtn.layer.shadowRadius = addBtn.frame.size.height / 2
    }
    
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addObject(_ sender: Any) {
        
        if(nameTxt.text != nil && nameTxt.text != "" && selectImage != nil){
            
            let obj = CircleObject([:])
            obj.name = nameTxt.text
            obj.circleID = K.Us.currentCircle?.uid
            obj.image = imageTxt.text
            K.Us.currentCircle?.addObject(object: obj)
            
            let alert = UIAlertController(title: "Save", message: "Your object was added.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    self.dismiss(animated: true, completion: nil)
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    @objc func donePicker(){
        imageTxt.resignFirstResponder()
        objectImage.image = selectImage
        
    }
    
    @objc func cancel(){
        imageTxt.resignFirstResponder()
        
    }
    
    // This function sets the text of the picker view to the content of the "salutations" array
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return names[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        imageTxt.text = names[row]
        
        selectImage = images[row]
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return names.count
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
