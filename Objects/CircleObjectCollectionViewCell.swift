//
//  CircleObjectCollectionViewCell.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 9/10/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit

class CircleObjectCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var circleObjectImage: UIImageView!
    
    @IBOutlet weak var cicleObjectLabel: UILabel!
    
    @IBOutlet weak var circleObjectView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        self.circleObjectImage.layoutIfNeeded() // Add this line
        circleObjectImage.circleImage()
        circleObjectView.roundCorners(radius: 4.0)
        circleObjectView.mainshadow(color:K.UI.shadow_color)
        
    }

}
