//
//  Object.swift
//  DocDocPatient
//
//  Created by juan esteban chaparro on 21/10/17.
//  Copyright © 2017 Tres Astronautas. All rights reserved.
//

import Foundation
import Firebase

class Object: NSObject {
    
    
    var originalDictionary: [String:Any]
    var uid: String?
    
    
    public init(_ dict: [String: Any]) {
        originalDictionary = dict
        
        if let uid = dict["id"] as? String {
            self.uid = uid
        }
    }
    
    public func save(route: String) {
        if uid == nil {
            let saving_ref = K.FireStore.ref().collection(route).addDocument(data: originalDictionary)
            uid = saving_ref.documentID
            
            saving_ref.updateData(["lastUpdated": FieldValue.serverTimestamp(), "id": uid!])
        } else {
            
            originalDictionary["id"] = uid!
            K.FireStore.ref().collection(route).document(uid!).setData(self.originalDictionary, merge: true, completion: { (err) in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            })
            K.FireStore.ref().collection(route).document(self.uid!).updateData(["lastUpdated": FieldValue.serverTimestamp()], completion: { (err) in
                
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            })
           
        }
    }
    
}

