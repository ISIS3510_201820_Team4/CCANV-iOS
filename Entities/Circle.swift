//
//  Circle.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 10/7/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import Foundation
import Firebase

class Circle: Object {
    
    static let collectionName = "circles"
    
    
    class func withID(id: String, callback: @escaping (_ s: Circle?)->Void) {
        K.FireStore.ref().collection(collectionName).document(id).getDocument { (document, error) in
            if error != nil {
                print(error!.localizedDescription)
            } else if document != nil && document!.exists {
                if let documentData = document?.data() {
                    callback(Circle(documentData))
                } else {
                    callback(nil)
                }
            } else {
                callback(nil)
            }
        }
    }
    
    class func WithRef(ref: DocumentReference , callback: @escaping (_ s: Circle?)->Void){
        ref.getDocument(completion: { (doc, error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            else if  doc != nil && doc!.exists  {
                if let documentData = doc?.data() {
                    callback(Circle(documentData))
                } else {
                    callback(nil)
                }
            }else {
                callback(nil)
            }
            
        })
    }
    
    
    public override init(_ dict: [String: Any]){
        super.init(dict)
        
        if let leaderID = dict["leaderID"] as? String{
            self.leaderID = leaderID
        }
        if let name = dict["name"] as? String {
            self.name =  name
        }
        if let location = dict["location"] {
            self.location = (location as? GeoPoint)
        }
        
        
    }
    
    
    override func save(route: String) {
        //if (self.uid != getCurrentUserUid()) {
        //  return
        // }
        
        if self.leaderID != nil {
            originalDictionary["leaderID"] = self.leaderID
        }
        if self.location != nil {
            originalDictionary["location"] = self.location
        }
        if self.name != nil {
            originalDictionary["name"] = self.name
        }
        
        
        
        super.save(route: Circle.collectionName)
    }
    
    
    public func prepareForSave() -> [String: Any] {
        //if (self.uid != getCurrentUserUid()) {
        //  return
        // }
        if self.leaderID != nil {
            originalDictionary["leaderID"] = self.leaderID
        }
        if self.location != nil {
            originalDictionary["location"] = self.location
        }
        if self.name != nil {
            originalDictionary["name"] = self.name
        }
       
        
        
        
        return originalDictionary
    }
    
    public func getObjects(callback: @escaping (_ s: [CircleObject]?)->Void){
        
        var consultations:[CircleObject] = []
        if let u = self.uid {
            K.FireStore.ref().collection("circles").document(u).collection("objects").getDocuments() { (querySnapshot, err) in
                
                if let err = err {
                    print("Error getting documents: \(err)")
                } else if querySnapshot?.isEmpty == false  {
                    for document in querySnapshot!.documents {
                        if let documentData = document.data() as [String:Any]? {
                            consultations.append(CircleObject(documentData))
                        }
                    }
                    callback(consultations)
                }else {
                    callback(consultations)
                }
                
            }
        }
        
        
    }
    
    
    public func getHistory(callback: @escaping (_ s: [Event]?)->Void){
        
        var consultations:[Event] = []
        if let u = self.uid {
            K.FireStore.ref().collection("circles").document(u).collection("history").getDocuments() { (querySnapshot, err) in
                
                if let err = err {
                    print("Error getting documents: \(err)")
                } else if querySnapshot?.isEmpty == false  {
                    for document in querySnapshot!.documents {
                        if let documentData = document.data() as [String:Any]? {
                            consultations.append(Event(documentData))
                        }
                    }
                    callback(consultations)
                }else {
                    callback(consultations)
                }
                
            }
        }
        
        
    }

    
    public func getParticipants() -> [UserParticipant]? {
        var add_participants:[UserParticipant] = []
        if let participants = originalDictionary["participants"] {
            
            if let addDict = participants as? [String:AnyObject] {
                for (_, pay) in addDict {
                    if let payDict = pay as? [String:AnyObject] {
                        add_participants.append(UserParticipant(payDict))
                    }
                }
                return add_participants
            }
            if let addArray = participants as? [[String:AnyObject]] {
                for pay in addArray {
                    add_participants.append(UserParticipant(pay))
                }
                return add_participants
            }
        }
        return nil
    }
    
    
    public func addParticipant(participant : UserParticipant){
        var allDictionaries : [Any] = []
            if var pl = getParticipants(){
                
                for p in pl{
                    
                    allDictionaries.append(p.prepareForSave() as Any)
                }
            }
        
            allDictionaries.append(participant.prepareForSave() as Any)
            originalDictionary["participants"] = allDictionaries
        
    
    }
    
    
    public func addObject(object : CircleObject){
        
        if let u = self.uid { K.FireStore.ref().collection("circles").document(u).collection("objects").addDocument(data: object.prepareForSave()) { (error) in
            if error != nil {
                print( error?.localizedDescription ?? "nada")
                
            }else {
                print("object Saved")
            }
            }
        }
    }
    
    var leaderID: String?
    var location: GeoPoint?
    var name : String?
    
    
    
    
    
    
}
