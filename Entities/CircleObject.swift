//
//  CircleObject.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 10/7/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import Foundation
import Firebase

class CircleObject: Object {
    
    static let collectionName = "objects"
    
    
    class func withID(id: String, callback: @escaping (_ s: CircleObject?)->Void) {
        K.FireStore.ref().collection(collectionName).document(id).getDocument { (document, error) in
            if error != nil {
                print(error!.localizedDescription)
            } else if document != nil && document!.exists {
                if let documentData = document?.data() {
                    callback(CircleObject(documentData))
                } else {
                    callback(nil)
                }
            } else {
                callback(nil)
            }
        }
    }
    
    class func WithRef(ref: DocumentReference , callback: @escaping (_ s: CircleObject?)->Void){
        ref.getDocument(completion: { (doc, error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            else if  doc != nil && doc!.exists  {
                if let documentData = doc?.data() {
                    callback(CircleObject(documentData))
                } else {
                    callback(nil)
                }
            }else {
                callback(nil)
            }
            
        })
    }
    
    
    public override init(_ dict: [String: Any]){
        super.init(dict)
        
        if let image = dict["image"] as? String{
            self.image = image
        }
        if let name = dict["name"] as? String {
            self.name =  name
        }
        if let circleID = dict["circleID"] as? String {
            self.circleID = circleID
        }
        
        
    }
    
    
    override func save(route: String) {
        //if (self.uid != getCurrentUserUid()) {
        //  return
        // }
        
        if self.circleID != nil {
            originalDictionary["circleID"] = self.circleID
        }
        if self.image != nil {
            originalDictionary["image"] = self.image
        }
        if self.name != nil {
            originalDictionary["name"] = self.name
        }
        
        
        
        super.save(route: route)
    }
    
    
    public func prepareForSave() -> [String: Any] {
        //if (self.uid != getCurrentUserUid()) {
        //  return
        // }
        if self.image != nil {
            originalDictionary["image"] = self.image
        }
        if self.circleID != nil {
            originalDictionary["circleID"] = self.circleID
        }
        if self.name != nil {
            originalDictionary["name"] = self.name
        }
        
        
        
        
        return originalDictionary
    }
   
    
    var circleID: String?
    var image: String?
    var name : String?
    
    
    
    
    
    
}
