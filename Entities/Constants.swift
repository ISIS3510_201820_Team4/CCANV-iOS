//
//  Constants.swift
//  DocDocPatient
//
//  Created by juan esteban chaparro on 21/10/17.
//  Copyright © 2017 Tres Astronautas. All rights reserved.
//

import Foundation
import Firebase
import MBProgressHUD
import RestEssentials

struct K {
    struct Test {
        static var test_val:Bool = true
        
        static func test_func() {
            _ = false
        }
        
    }
    
    struct Helper {
        static let fb_date_short_format:String = "dd-MM-yyyy"
        static let fb_date_format:String = "yyyy-MM-dd'T'HH:mmxxxxx"
        static let fb_date_medium_format:String = "yyyy-MM-dd'T'HH:mm:ssxxxxx"
        static let fb_long_date_format: String = "yyyy-MM-dd'T'HH:mm:ss.SSSxxxxx"
        static let fb_time_format: String = "hh:mm a"
        static let fb_time_ymd: String = "yyyy-MM-dd"
    }
    
    struct Database {
        public static func ref() -> DatabaseReference {
            return Firebase.Database.database().reference()
        }
        private static let storageURL: String = ""
        public static func storageRef() -> StorageReference {
            return Storage.storage().reference(forURL: storageURL)
        }
    }
    
    struct FireStore {
        public static func ref() -> Firestore {
            return Firestore.firestore()
        }
    }
    
    
    struct Cloud {
//         public static let cludURL: String = ""
//         public static let cludToken: String = ""
        
    }
    struct UI {
        static let main_color: UIColor = UIColor(netHex: 0x6be778)
        static let second_color: UIColor = UIColor(netHex: 0xbcf386)
        static let third_color: UIColor = UIColor(netHex: 0xb8b8b8)
        static let shadow_color: UIColor = UIColor(netHex: 0xb8b8b8)
        static let round_px: CGFloat = 25.0
        
    }
    
    struct Cache {
        
//        static var symptoms:[SintomsPicker]?
//        static let imageCache = NSCache<NSString, UIImage>()
//        static let imaCache = NSCache<NSString, UIView>()
//
//        static var dates = [Date]()
//
//
//
//        public static func getAvailableDates( callback: @escaping (_ s: [Date]?)->Void){
//
//
//            if dates.count == 0 || dates[0].timeIntervalSince(Date()) < -3600 {
//
//
//                    guard let rest = RestController.make(urlString: K.Cloud.cludURL + "book3AAvailableDates?results=200" ) else {
//                        print("Bad URL")
//                        return
//                    }
//
//                    var options = RestOptions()
//                    let authToken = String(format: "Bearer %@", K.Cloud.cludToken)
//                    options.httpHeaders = ["Authorization": authToken , "Content-Type": "application/json"]
//
//                    let postData: JSON = []
//
//                    rest.post(postData, options: options) { (result, httpResponse) in
//                        //            print("respond Status  : \(httpResponse?.statusCode)")
//                        if httpResponse?.statusCode == 200 {
//                            // Payment succesfull
//                            do {
//                                let json = try result.value()
//
//                                DispatchQueue.main.async {
//
//                                    //                        print(json.array)
//                                    let dateFormatter = DateFormatter()
//                                    dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
//                                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
//                                    if let array =  json.array {
//                                        for ob in array {
//                                            //                                print(ob.string)
//
//                                            guard let date = dateFormatter.date(from: ob.string! ) else {
//                                                fatalError("ERROR: Date conversion failed due to mismatched format.")
//                                            }
//                                            dates.append(date)
//                                        }
//                                    }
//
//                                    callback(dates)
//                                }
//                            } catch {
//                                DispatchQueue.main.async {
//
//                                    print("Error performing GET: \(error)")
//                                    callback(nil)
//                                }
//
//                            }
//                        }else {
//                            DispatchQueue.main.async {
//                                do {
//                                    print( try result.value())
//                                } catch {
//
//                                }
//                                callback(nil)
//                            }
//                        }
//                    }
//            }
//            else if (dates.count > 0 && dates[0].timeIntervalSince(Date()) > 0){
//
////                print(" initial date : \(dates[0].description) , current Date: \(Date().description), diference : \(dates[0].timeIntervalSince(Date()).description)")
//                callback(dates)
//            }
//
//
//
//        }
//       public static func downloadSymptoms() {
//            var dataDownload = [SintomsPicker] ()
//            K.FireStore.ref().collection("metadata").document("metadata").collection("symptoms").getDocuments() { (querySnapshot, err) in
//                if let err = err {
//                    print("Error getting documents: \(err)")
//                } else if querySnapshot?.isEmpty == false  {
//
//                    for document in querySnapshot!.documents {
//                        if let documentData = document.data() as [String:Any]? {
//                            dataDownload.append(SintomsPicker(documentData))
//                        }
//                    }
//                    symptoms = dataDownload
//
//                }
//
//            }
//
//        }
//
//        static func test(string: String, view: UIView){
//
//
//            if let cachedImage = imaCache.object(forKey: string as NSString) {
//                print("object cached")
//                print(cachedImage)
//            } else {
//                print("guarda cached")
//                imaCache.setObject(view, forKey: string as NSString)
//            }
//
//        }
//
//            static func downloadImage(link: String, completion: @escaping (_ image: UIImage?, _ error: Error? ) -> Void) {
//
//                guard let url = URL(string: link) else { return }
//                if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) {
//                    print("EN CACHE !!!!!!!!!!!!!!!!!!!!!")
//                    completion(cachedImage, nil)
//                } else {
//
//                    URLSession.shared.dataTask(with: url) { (data, response, error) in
//                        guard
//                            let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
//                            let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
//                            let data = data, error == nil,
//                            let image = UIImage(data: data)
//                            else { return }
//                        DispatchQueue.main.async() { () -> Void in
//                            print("GUARDA EN CACHE !!!!!!!!!!!!!!!!!!!!!")
//                            imageCache.setObject(image, forKey: link as NSString)
//                            completion(image, nil)
//                        }
//                        }.resume()
//
//                }
//            }
//
        
    }
    
    struct Us {
        
        static var user:User?
        static var currentCircle: Circle?
        static func reloadUser() {

            if user != nil {
                User.withID(id: user!.uid!, callback: { (c) in
                    K.Us.user = c
                })
            }

        }

        static func logged_user () -> Firebase.User?{
            return Auth.auth().currentUser
        }
        
    }
    
}

func getCurrentUserUid()->String?{
   return Auth.auth().currentUser?.uid
}

func getCurrentUserMail()->String?{
    return Auth.auth().currentUser?.email
}



